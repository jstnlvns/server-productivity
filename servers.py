#! /usr/bin/python
v = "v1.0"

import os

# the app looks in the user's home directory for the server list file
serverfile = str(os.environ['HOME']) + '/svrs.txt'

# opening server list text file
ifile = open(serverfile,'rU')

b = '-----------------------------------------------------------------'
sp = (len(b)-12) * " "

print '\nServers' + sp + v
print b
# creating an empty array.  this will be populated with all the servers from text file.
servers = []

# Reading each line in my server list file and using the .split function to tell python where my file is delimited.  My server list file is comma delimited but you may use another delimiter.
for line in ifile:
	servers.append(line.split(','))

for j in servers:
	iplen = len(j[1])
	srvnum = j[0]
	lensrvnum = len(str(j[0]))
	outsrvnum = ''
	if lensrvnum > 1:
		outsrvnum = str(srvnum)
	else:
		outsrvnum = str(srvnum) + ' '
	ipsp = (len(b) - iplen) * " "
	namelen = len(j[2])
	namesp = ((len(b) - namelen) - 20) * " "
	serverlistline = "{} | {}{}| {}".format(outsrvnum,j[2],namesp,j[1])
	print serverlistline

servers_dict = dict()

for i in servers:
    servers_dict[i[0]] = [i[1],i[2]]

tac = "y"
serverid = ""

while tac =="y":
   serverid = raw_input("\nInput server number:  ")
   if servers_dict.has_key(serverid) == True:
      tac = "n"
      servercommand = servers_dict[serverid][0]
      notification = "Connection to {}".format(servers_dict[serverid][1])
      print notification
   else:
      print "\nZoinks!  You entered an incorrect paramenter.\n"
      ta = raw_input("Try again? (Y/N): ")
      tac = ta.lower()
      if tac != "y":
         print "\nNow Exiting.\n"
         quit()

print b
sshcommand = "ssh -lCY justin@{}".format(servercommand)
os.system(sshcommand)